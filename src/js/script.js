// General
var general = {
  globalID: 0,
  todos   : []
  // todoTemp: []
}

function TodoList() {
  this.$construct = this.construct()
  this.bindEvents() 
}
TodoList.prototype.construct = function() {
  this.$elem    = this.buildElem()
  this.$stroage = this.localStorage()
}

TodoList.prototype.localStorage = function() {
  console.log(general.todos)

  var temp = general.todos
  for (var i = 0; i < temp.length; i ++) {

    if (temp[i].clean == false) {
      var id     = temp[i].id,
          title  = temp[i].title

      var newTemp = new TodoItem(id, title)
      general.todos.push(newTemp)

      var $ul  = this.$elem.querySelector('ul')
      $ul.append(newTemp.$elem)
      console.log(general.todos)
    }
  }
}

TodoList.prototype.buildElem = function() {
  var $ul        = document.createElement('ul'),
      $input     = document.createElement('input'),
      $container = document.createElement('div'),
      $todoNew   = document.createElement('div'),
      $date      = document.createElement('h1')

  $container.setAttribute('class', 'task')
  $todoNew.setAttribute('class', 'input-container')
  $ul.setAttribute('class', 'task-container')

  $input.setAttribute('type', 'text')
  $input.setAttribute('class', 'input')
  $input.setAttribute('placeholder', 'My new task')
  $todoNew.append($input)


  // 加入時間要素，尚未調整時差
  var today = new Date()

  $date.setAttribute('class', 'date')
  $date.textContent = today.getDate() + '/' + today.getMonth() + ' ' + today.getFullYear() + ' 星期' + today.getDay();

  $container.append($date)
  $container.append($todoNew)
  $container.append($ul)

  return $container
}
TodoList.prototype.bindEvents = function() {
  var that = this;
  var $input = that.$elem.querySelector('.input');
  $input.addEventListener('keydown', function (e) {
      if (e.keyCode === 13 && $input.value != '') {
        that.onCreateTodo();

        // Clear input
        $input.value = '';
      }
  });

}
TodoList.prototype.onCreateTodo = function() {
  // 建立 newTodo
  var input     = this.$elem.querySelector('input')
  var ul        = this.$elem.querySelector('ul')
  var title     = input.value
  var id = general.globalID ++
  var newTodo   = new TodoItem(id, title)
  ul.appendChild(newTodo.$elem)

  // 將新 Todo 推到 todos 陣列 
  general.todos.push(newTodo)

  console.log(general.todos)

  // reset input value
  input.value = ""
}
function TodoItem(id, title) {

  this.id = id
  this.title = title
  this.done = false
  this.clean = false
  this.$elem = this.buildElem(id, title)
  this.bindEvents()
  return this
}
TodoItem.prototype.buildElem = function(id, title) {
  var $li       = document.createElement('li'),
      $label    = document.createElement('label'),
      $title    = document.createElement('div'),
      $checkbox = document.createElement('input'),
      $btn      = document.createElement('button')

  $label.setAttribute('for', id)
  $label.setAttribute('class', 'label')
  $checkbox.setAttribute('type', 'checkbox')
  $checkbox.setAttribute('class', 'checkbox')
  $checkbox.setAttribute('id', id)
  $title.setAttribute('class', 'content')
  $title.textContent = title
  $btn.setAttribute('class', 'del')
  $btn.textContent = 'del'
  
  $li.setAttribute('class', 'task-row')
  $li.append($checkbox)
  $li.append($label)
  $li.append($title)
  $li.append($btn)

  return $li
}
TodoItem.prototype.bindEvents = function() {
  var button = this.$elem.querySelector('button')
  button.addEventListener('click', this.onRemoveTodo.bind(this))
}
TodoItem.prototype.onRemoveTodo = function() {
  var todoArray = general.todos,
      todoIndex = todoArray.indexOf(this)

  todoArray[todoIndex].$elem.remove()
  todoArray[todoIndex].clean = true
  console.log(todoArray[todoIndex])
  console.log(todoArray)
}
// Begin this APP
function App() {
  this.createItem()
}

App.prototype.createItem = function () {
  var item = new TodoList()
  var $container = document.getElementById('container')
  $container.appendChild(item.$elem)
}
window.app = new App()

